package me.mistercat.manager;

import java.util.ArrayList;
import java.util.UUID;
import java.util.function.Consumer;

import org.bukkit.entity.Player;

import com.mongodb.BasicDBObject;

import de.fkfabian.api.SocketAPI.SocketAPI;
import de.fkfabian.api.mc.util.UUIDFetcher;
import me.mistercat.reportsystem.ReportSystem;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class ReportManager {

	private ArrayList<String> list;

	public ReportManager() {
		list = new ArrayList<>();
		list.add("Chatverhalten");
		list.add("Hacking");
	}

	public ArrayList<String> getList() {
		return list;
	}

	public boolean existsReportReason(String reason) {

		for (String reasons : list) {
			if (reasons.toUpperCase().equalsIgnoreCase(reason.toUpperCase())) {
				return true;
			}

		}
		return false;

	}

	public void reportPlayer(Player reporter, UUID reported, String reason) {
		
		String reportedName = UUIDFetcher.getNameSync(reported);
		
		reporter.sendMessage(ReportSystem.getInstance().getPrefix() + "Du hast erfolgreich §e"
				+ reportedName + " §agemeldet!");

		TextComponent component = new TextComponent(
				ReportSystem.getInstance().getPrefix() + "Es wurde ein Spieler gemeldet! §8[§e"+reportedName+"§8]");
		component.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/reportlist"));
		component.setHoverEvent(new HoverEvent(net.md_5.bungee.api.chat.HoverEvent.Action.SHOW_TEXT,
				new ComponentBuilder("§aKlick hier, um die Reports zu öffnen!").create()));

		
		
		ReportSystem.getInstance().getTeamManager().sendComponentToTeam(component);
		

		ReportSystem.getInstance().getMongoDB().getData(reported.toString(),
				new Consumer<BasicDBObject>() {
					@SuppressWarnings("unchecked")
					@Override
					public void accept(BasicDBObject t) {

						if (t != null) {

							ArrayList<String> reporterList;
							ArrayList<String> reasonsList;


							
							reporterList = (ArrayList<String>) t.get("reporter");
							reasonsList = (ArrayList<String>) t.get("reasons");

					
								
							
							
							if(!reasonsList.contains(reason)) {
								reasonsList.add(reason);
							}
							reporterList.add(reporter.getUniqueId().toString());
							
							if(t.getString("status").equalsIgnoreCase("COMPLETE")) {
							ReportSystem.getInstance().getMongoDB().setData(false, reported.toString(),
									reasonsList, reporterList, "WAITING", "nothing");
							}else{
								ReportSystem.getInstance().getMongoDB().setData(true, reported.toString(),
										reasonsList, reporterList, "WAITING", "nothing");
							}
						} else {
							ArrayList<String> reporterList = new ArrayList<>();
							ArrayList<String> reasonsList = new ArrayList<>();

							reporterList.add(reporter.getUniqueId().toString());
							reasonsList.add(reason);
							ReportSystem.getInstance().getMongoDB().getCompleteData(new Consumer<ArrayList<BasicDBObject>>() {

								@Override
								public void accept(ArrayList<BasicDBObject> t) {
					
									ReportSystem.getInstance().getMongoDB().setData(false, reported.toString(),
											reasonsList, reporterList, "WAITING", "nothing");
								}
							});;

						}
					}
				});

	}
	public void acceptReport(UUID uuidReported, Player teamler) {
		
		if(ReportSystem.getInstance().getManager().isOnline(uuidReported)) {
		
			ReportSystem.getInstance().getMongoDB().getData(uuidReported.toString(), new Consumer<BasicDBObject>() {

				@SuppressWarnings("unchecked")
				@Override
				public void accept(BasicDBObject t) {
					if(t != null) {
						
						ArrayList<String> reporterList;
						ArrayList<String> reasonsList;

						reporterList = (ArrayList<String>) t.get("reporter");
						reasonsList = (ArrayList<String>) t.get("reasons");
						
						
						ReportSystem.getInstance().getMongoDB().setData(true, uuidReported.toString(), reasonsList, reporterList, "PROGRESS", teamler.getUniqueId().toString());
						teamler.sendMessage(ReportSystem.getInstance().getPrefix() + "§aDu wirst zum Spieler gesendet!");
						ReportSystem.getInstance().getTeamManager().sendServer(teamler, uuidReported);
						
						for(String uuids : reporterList) {
							if(uuids != null) {
								if(UUIDFetcher.isOnlineSync(UUID.fromString(uuids))) {
									SocketAPI.sendMessageToPlayer(UUID.fromString(uuids), ReportSystem.getInstance().getPrefix()+"§aDein Report wurde von §e"+teamler.getName()+" §aangenommen!");
								}
							}
							
						}
						
						
					}else{
						teamler.sendMessage(ReportSystem.getInstance().getPrefix() + "§4Der Spieler konnte nicht mehr gefunden werden!");
					}
						
						
				}
			});
			
		}else{
			teamler.sendMessage(ReportSystem.getInstance().getPrefix()+ "§4Dieser Spieler ist nicht mehr online!");
		}
		
		
		
		
	}
	public void endReport(UUID uuidReported, Player teamler) {
		
		ReportSystem.getInstance().getMongoDB().getData(uuidReported.toString(), new Consumer<BasicDBObject>() {

			@SuppressWarnings("unchecked")
			@Override
			public void accept(BasicDBObject t) {
				
				if(t != null) {
					ArrayList<String> reporterList;
					ArrayList<String> reasonsList;
					
					
					reporterList = (ArrayList<String>) t.get("reporter");
					reasonsList = (ArrayList<String>) t.get("reasons");
		
					
					ReportSystem.getInstance().getMongoDB().setData(true, uuidReported.toString(), reasonsList, reporterList, "COMPLETE", teamler.getUniqueId().toString());
					teamler.sendMessage(ReportSystem.getInstance().getPrefix() + "Du hast den Report beendet!");
					
					
				}
				
				
			}
		});
		
	}

}
