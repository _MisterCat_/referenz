package me.mistercat.manager;


import java.util.UUID;

import org.bukkit.entity.Player;


import de.fkfabian.api.SocketAPI.SocketAPI;

import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;

public class TeamManager {

	public void sendToTeam(String msg) {

		SocketAPI.broadcastMessage("playhills.reports", msg);
	}

	public void sendComponentToTeam(TextComponent component) {

		SocketAPI.sendBroadcastCoponentMessage("playhills.reports", ComponentSerializer.toString(component));

	}

	public void sendServer(Player teamler, UUID player) {

		SocketAPI.sendUUIDToUUID(teamler.getUniqueId(), player); 
	}


}
