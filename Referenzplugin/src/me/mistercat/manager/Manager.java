package me.mistercat.manager;

import java.util.UUID;
import de.fkfabian.api.mc.util.UUIDFetcher;

public class Manager {

	public boolean isOnline(UUID uuid) {
		
		if(uuid == null) {
			return false;
		}
		
		return UUIDFetcher.isOnlineSync(uuid);
	}
	
}
