package me.mistercat.manager;

import java.util.HashMap;

import org.bukkit.entity.Player;

public class Report {

	Player reported;
	Player reporter;
	String reason;
	HashMap<String, String> reports = new HashMap<>();

	public Report(Player reported, Player reporter, String reason) {
		this.reported = reported;
		this.reporter = reporter;
		this.reason = reason;
		reports.put(reporter.getUniqueId().toString(), reason);
	}

	public String getReason() {
		return reason;
	}

	public Player getReported() {
		return reported;
	}

	public Player getReporter() {
		return reporter;
	}

	public HashMap<String, String> getReports() {
		return reports;
	}
}
