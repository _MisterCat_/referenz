package me.mistercat.inventory;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import de.fkfabian.api.mc.util.ItemBuilder;
import de.fkfabian.api.mc.util.UUIDFetcher;

public class AcceptCloseInventory {

	public Inventory getInventory(UUID reportedplayerName) {
		
		Inventory inventory = Bukkit.createInventory(null, 9, "§4Report beenden");
		ItemStack banPlayer = new ItemBuilder(Material.EMERALD_BLOCK).withName("§aReport annehmen").withLore("§eKlicke hier, um den Report zu beenden!").toItemStack();
		ItemStack cancelReport = new ItemBuilder(Material.REDSTONE_BLOCK).withName("§cReport beenden").withLore("§eKlicke hier, wenn der Spieler keinen Regelversto� begangen hat.").withLore("§eOder klicke hier, wenn der Spieler das Spiel verlassen hat!").toItemStack();
		ItemStack playerHead = new ItemBuilder(Material.SKULL_ITEM).withName("§e§l" + UUIDFetcher.getNameSync(reportedplayerName)).toPlayerHead(UUIDFetcher.getNameSync(reportedplayerName));
		
		inventory.setItem(0, banPlayer);
		inventory.setItem(8, cancelReport);
		inventory.setItem(4, playerHead);
		
		return inventory; 
		
		
	}
}
