package me.mistercat.inventory;

import java.util.ArrayList;
import java.util.UUID;
import java.util.function.Consumer;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.libs.joptsimple.internal.Strings;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.mongodb.BasicDBObject;

import de.fkfabian.api.mc.util.UUIDFetcher;
import me.mistercat.reportsystem.ReportSystem;
import me.mistercat.utils.ItemBuilder;
import net.md_5.bungee.api.ChatColor;

public class ReportlistInventory {

	public Inventory getInventory() {
		Inventory inventory = Bukkit.createInventory(null, 54, "§aReports");

		ReportSystem.getInstance().getMongoDB().getAllData(new Consumer<ArrayList<BasicDBObject>>() {

			@Override
			public void accept(ArrayList<BasicDBObject> t) {

				for (BasicDBObject obj : t) {

					if (obj == null) {
						continue;
					}

					String name = UUIDFetcher.getNameSync(UUID.fromString(obj.getString("uuid")));
					@SuppressWarnings("unchecked")
					ArrayList<String> reasons = (ArrayList<String>) obj.get("reasons");

					String lore1 = Strings.join(reasons, "�a, ");

					ItemStack reportedPlayer = new ItemBuilder(Material.SKULL_ITEM).withName("§eSpieler: §a§l" + name)
							.withLore("§eReports: §a§l" + reasons.size()).withLore("§eGründe: §a" + lore1)
							.withLore("§eKlicken, um den Report anzunehmen").toPlayerHead(ChatColor.stripColor(name));
					inventory.addItem(reportedPlayer);
				}

			}
		});

		return inventory;
	}

}
