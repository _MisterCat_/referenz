package me.mistercat.listener;

import java.util.ArrayList;
import java.util.UUID;
import java.util.function.Consumer;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scheduler.BukkitRunnable;

import com.mongodb.BasicDBObject;

import de.fkfabian.api.mc.util.UUIDFetcher;
import me.mistercat.manager.Report;
import me.mistercat.reportsystem.ReportSystem;

public class InventoryClick implements Listener {

	@EventHandler
	public void onClick(InventoryClickEvent event) {

		if (event.getInventory().getTitle() == null)
			return;
		if (event.getCurrentItem() == null)
			return;
		if (event.getClickedInventory() == null)
			return;
		if (event.getInventory() == null)
			return;
		if(event.getSlotType() == null) 
			return;
		
		
		
		if (event.getInventory().getName().equalsIgnoreCase("§4Report beenden")) {
			event.setCancelled(true);
			SkullMeta meta = (SkullMeta) event.getInventory().getItem(4).getItemMeta();
			UUIDFetcher.getUUIDAsync(meta.getOwner(), new Consumer<UUID>() {

				@Override
				public void accept(UUID uuid) {
					if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aReport annehmen")) {

						ReportSystem.getInstance().getMongoDB().getData(uuid.toString(), new Consumer<BasicDBObject>() {
							

							@SuppressWarnings("unchecked")
							@Override
							public void accept(BasicDBObject obj) {

								if (obj == null) {
									event.getWhoClicked().sendMessage(ReportSystem.getInstance().getPrefix()
											+ "§4Der Report konnte nicht mehr gefunden werden!");
									return;
								}

								ArrayList<String> reporterList;
								ArrayList<String> reasonsList;
			

								reporterList = (ArrayList<String>) obj.get("reporter");
								reasonsList = (ArrayList<String>) obj.get("reasons");
	

								
								ReportSystem.getInstance().getMongoDB().setData(true, uuid.toString(), reasonsList,
										reporterList, "COMPLETE", event.getWhoClicked().getUniqueId().toString());
								event.getWhoClicked()
										.sendMessage(ReportSystem.getInstance().getPrefix() + "Du hast den Report beendet!");
								
								event.getView().close();

							}

						});

					} else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cReport beenden")) {
						event.setCancelled(true);
						ReportSystem.getInstance().getMongoDB().getData(uuid.toString(), new Consumer<BasicDBObject>() {

							@Override
							public void accept(BasicDBObject t) {
								if (t == null) {
									event.getWhoClicked().sendMessage(ReportSystem.getInstance().getPrefix()
											+ "§4Der Report konnte nicht mehr gefunden werden!");
									return;
								}
								event.getWhoClicked()
										.sendMessage(ReportSystem.getInstance().getPrefix() + "Du hast den Report beendet!");
								ReportSystem.getInstance().getMongoDB().getCollection().remove(t);
								event.getView().close();
							}

						});

					}

				}
			});


		} else if (event.getInventory().getName().equalsIgnoreCase("§aReports")) {
			event.setCancelled(true);
			ItemStack clickedStack = event.getCurrentItem();
			SkullMeta meta = (SkullMeta) clickedStack.getItemMeta();
			String name = meta.getOwner();
			Player player = (Player) event.getWhoClicked();
			UUIDFetcher.getUUIDAsync(name, new Consumer<UUID>() {

				@Override
				public void accept(UUID t) {

					ReportSystem.getInstance().getReportManager().acceptReport(t, player);
					new BukkitRunnable() {
						
						@Override
						public void run() {
							event.getView().close();
							
						}
					}.runTask(ReportSystem.getInstance());
				}
		
				
			
			});



		}

	}
	
}
