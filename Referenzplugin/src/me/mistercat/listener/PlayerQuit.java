package me.mistercat.listener;

import java.util.UUID;
import java.util.function.Consumer;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.mongodb.BasicDBObject;

import me.mistercat.reportsystem.ReportSystem;

public class PlayerQuit implements Listener {

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {

		ReportSystem.getInstance().getMongoDB().getData(event.getPlayer().getUniqueId().toString(),
				new Consumer<BasicDBObject>() {

					@Override
					public void accept(BasicDBObject t) {
						if (t != null) {

							String status = t.getString("status");
							
							
							if(status.equalsIgnoreCase("WAITING")) {
							ReportSystem.getInstance().getMongoDB().getCollection().remove(t);
							}else if(status == "PROGRESS") {
							
								UUID uuid = UUID.fromString(t.getString("teamuuid"));
								Player teamler = Bukkit.getPlayer(uuid);
								teamler.sendMessage(ReportSystem.getInstance().getPrefix()+"Der Spieler ist nicht mehr im Spiel!");
								
							}
						}

					}
				});

	}

}
