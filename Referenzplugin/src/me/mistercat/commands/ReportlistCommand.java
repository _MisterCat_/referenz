package me.mistercat.commands;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.scheduler.BukkitRunnable;

import com.mongodb.BasicDBObject;

import me.mistercat.reportsystem.ReportSystem;

public class ReportlistCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (!(sender instanceof Player))
			return true;
		Player player = (Player) sender;
		if (player.hasPermission("playhills.reports")) {

			if (args.length == 0) {

				AtomicInteger i = new AtomicInteger(0);

				ReportSystem.getInstance().getMongoDB().getCompleteData(new Consumer<ArrayList<BasicDBObject>>() {

					@Override
					public void accept(ArrayList<BasicDBObject> t) {
						if (t == null || t.isEmpty()) {
							player.sendMessage(
									ReportSystem.getInstance().getPrefix() + "§4Es gibt keine offenen Reports!");
							return;
						}

						for (BasicDBObject obj : t) {

							String status = obj.getString("status");

							if (status.equalsIgnoreCase("PROGRESS")) {
						
								String uuid = obj.getString("teamuuid");
								if (player.getUniqueId().toString().equalsIgnoreCase(uuid)) {

									openInventorySync(player, ReportSystem.getInstance().getAcceptCloseInventory()
											.getInventory(UUID.fromString(obj.getString("uuid"))));

									player.sendMessage(ReportSystem.getInstance().getPrefix()
											+ "Möchtest du den Spieler bannen oder den Report abbrechen?");
									break;

								}

							} else if (status.equalsIgnoreCase("WAITING")) {
								i.set(1);

								Inventory inv = ReportSystem.getInstance().getReportlistInventory().getInventory();

								openInventorySync(player, inv);

								player.sendMessage(ReportSystem.getInstance().getPrefix()
										+ "Du hast alle aktiven Reports geöffnet!");
								break;

							} else {
								if (i.get() == 0) {
									player.sendMessage(ReportSystem.getInstance().getPrefix()
											+ "§4Es gibt keine offenen Reports!");
									
								}
							}

						}

					}
				});

			} else {
				player.sendMessage(ReportSystem.getInstance().getPrefix() + "§4Benutzung: §e/reportlist");
			}

		}

		return true;
	}

	public void openInventorySync(Player player, Inventory inv) {
		new BukkitRunnable() {

			@Override
			public void run() {
				player.openInventory(inv);
			}
		}.runTask(ReportSystem.getInstance());
	}

}
