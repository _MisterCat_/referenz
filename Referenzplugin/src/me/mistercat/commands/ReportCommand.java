package me.mistercat.commands;

import java.util.ArrayList;
import java.util.UUID;
import java.util.function.Consumer;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import de.fkfabian.api.mc.util.UUIDFetcher;
import me.mistercat.reportsystem.ReportSystem;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;

public class ReportCommand implements CommandExecutor {

	public ArrayList<String> wait = new ArrayList<>();

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (!(sender instanceof Player))
			return true;
		Player player = (Player) sender;

		if (args.length == 0) {
			player.sendMessage(ReportSystem.getInstance().getPrefix() + "§a/report §e<Spieler>");
		} else if (args.length == 1) {

			UUIDFetcher.getUUIDAsync(args[0], new Consumer<UUID>() {

				@Override
				public void accept(UUID t) {
					
					if(t == null) {
						player.sendMessage(
								ReportSystem.getInstance().getPrefix() + "§4Dieser Spieler ist nicht online.");
						return;
					}
					
					String target = UUIDFetcher.getNameSync(t);


					
					if (ReportSystem.getInstance().getManager().isOnline(t)) {

						if (t.toString().equalsIgnoreCase(player.getUniqueId().toString())) {
							player.sendMessage(
									ReportSystem.getInstance().getPrefix() + "§4Du kannst dich nicht selber melden!");
							return;
						}

						player.sendMessage(ReportSystem.getInstance().getPrefix() + "Für welchen Grund möchtest du §e"
								+ target + " §areporten?");
						ArrayList<String> list = ReportSystem.getInstance().getReportManager().getList();

						for (int i = 0; i < list.size(); i++) {

							TextComponent additionalComponent = new TextComponent();
							additionalComponent.setText("§7- §a" + list.get(i) + "");

							additionalComponent.setClickEvent(
									new ClickEvent(net.md_5.bungee.api.chat.ClickEvent.Action.RUN_COMMAND,
											"/reportsystem:report " + target + " " + list.get(i)));

							additionalComponent.setHoverEvent(new HoverEvent(Action.SHOW_TEXT, new ComponentBuilder(
									"§aKlicke hier, um §e" + target + "§a für \"" + list.get(i) + "\" zu melden!")
											.create()));
							player.spigot().sendMessage(additionalComponent);
						}
 
					} else {
						player.sendMessage(
								ReportSystem.getInstance().getPrefix() + "§4Dieser Spieler ist nicht online.");
					}
				}
			});

		} else if (args.length == 2) {

			UUIDFetcher.getUUIDAsync(args[0], new Consumer<UUID>() {

				@Override
				public void accept(UUID t) {

					if(t == null) {
						player.sendMessage(
								ReportSystem.getInstance().getPrefix() + "§4Dieser Spieler ist nicht online.");
						return;
					}
					
					String reason = args[1];


					
					if (ReportSystem.getInstance().getManager().isOnline(t)) {

						if (t.toString().equalsIgnoreCase(player.getUniqueId().toString())) {
							player.sendMessage(
									ReportSystem.getInstance().getPrefix() + "§4Du kannst dich nicht selber melden!");
							return;
						}

						if (wait.contains(player.getUniqueId().toString())) {
							player.sendMessage(ReportSystem.getInstance().getPrefix()
									+ "§4Bitte warte einen Moment, bis du wieder einen Spieler meldest!");
							return;
						}

						if (ReportSystem.getInstance().getReportManager().existsReportReason(reason)) {

							player.sendMessage(ReportSystem.getInstance().getPrefix()
									+ "§aBist du sicher, dass du diesen Spieler reporten möchtest?");
							TextComponent component = new TextComponent();
							component.setText(
									ReportSystem.getInstance().getPrefix() + "§8[§4§lJa, ich bin mir sicher§8]");
							component.setHoverEvent(new HoverEvent(Action.SHOW_TEXT,
									new ComponentBuilder("§4Klicke hier, wenn du dir sicher bist!").create()));
							component.setClickEvent(
									new ClickEvent(net.md_5.bungee.api.chat.ClickEvent.Action.SUGGEST_COMMAND,
											"/report confirm " + args[0] + " " + args[1]));
							player.spigot().sendMessage(component);

						} else {
							player.sendMessage(ReportSystem.getInstance().getPrefix()
									+ "§4Bitte gebe einen g�ltigen Grund an! §e-> §e/report");
						}

					} else {
						player.sendMessage(
								ReportSystem.getInstance().getPrefix() + "§4Dieser Spieler ist nicht online!");
					}
				}
			});

		} else if (args.length == 3) {
			if(args[0].equalsIgnoreCase("confirm")) {

				
				UUIDFetcher.getUUIDAsync(args[1], new Consumer<UUID>() {

					@Override
					public void accept(UUID t) {
					
						if(t == null) {
							player.sendMessage(
									ReportSystem.getInstance().getPrefix() + "§4Dieser Spieler ist nicht online.");
							return;
						}
						
						String reason = args[2];

						if (ReportSystem.getInstance().getManager().isOnline(t)) {

							if (t.toString().equalsIgnoreCase(player.getUniqueId().toString())) {
								player.sendMessage(
										ReportSystem.getInstance().getPrefix() + "§4Du kannst dich nicht selber melden!");
								return;
							}

							if (wait.contains(player.getUniqueId().toString())) {
								player.sendMessage(ReportSystem.getInstance().getPrefix()
										+ "§4Bitte warte einen Moment, bis du wieder einen Spieler meldest!");
								return;
							}

							if (ReportSystem.getInstance().getReportManager().existsReportReason(reason)) {

								ReportSystem.getInstance().getReportManager().reportPlayer(player, t, reason);
								
								wait.add(player.getUniqueId().toString());
								new BukkitRunnable() {

									@Override
									public void run() {
										if (wait.contains(player.getUniqueId().toString())) {
											wait.remove(player.getUniqueId().toString());
										}

									}
								}.runTaskLater(ReportSystem.getInstance(), 20 * 120);

							} else {
								player.sendMessage(ReportSystem.getInstance().getPrefix()
										+ "§4Bitte gebe einen gültigen Grund an! §e-> §e/report");
							}

						} else {
							player.sendMessage(
									ReportSystem.getInstance().getPrefix() + "§4Dieser Spieler ist nicht online!");
						}
					}
				});
				



				
		
			}
		}

		return true;
	}

}
