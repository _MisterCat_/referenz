package me.mistercat.database;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

@SuppressWarnings("deprecation")
public class MongoDB {

	private String ip;
	private Integer port;
	private MongoClient client;
	private DBCollection collection;

	private DB database;
	public Executor executor = Executors.newCachedThreadPool();

	public MongoDB(String ip, Integer port, String databasename) {
		this.ip = ip;
		this.port = port;
		MongoCredential credential = MongoCredential.createCredential("*ZENSIERT*", "*ZENSIERT*", "*ZENSIERT*".toCharArray());
		client = new MongoClient(new ServerAddress("ZENSIERT*", 27017), Arrays.asList(credential));
		database = client.getDB("dev");

	}

	public void connectMongoDB() {
		this.collection = database.getCollection("Reports");
	}

	public void getAllData(Consumer<ArrayList<BasicDBObject>> consumer) {

		getExecutor().execute(() -> {

			ArrayList<BasicDBObject> list = new ArrayList<>();

			DBObject d0 = new BasicDBObject("$match", new BasicDBObject("status", "WAITING"));
			DBObject d1 = new BasicDBObject("$project", new BasicDBObject("item", 1).append("uuid", "$uuid")
					.append("reasons", "$reasons").append("reporters", new BasicDBObject("$size", "$reporter")));
			DBObject d2 = new BasicDBObject("$sort", new BasicDBObject("reporters", -1));

			AggregationOutput found = getCollection().aggregate(d0, d1, d2);
			Iterator<DBObject> iterator = found.results().iterator();
			while (iterator.hasNext()) {
				BasicDBObject dbObject = (BasicDBObject) iterator.next();
				if (dbObject != null) {
					list.add(dbObject);
				}

			}

			consumer.accept(list);
		});

	}

	public void getCompleteData(Consumer<ArrayList<BasicDBObject>> consumer) {

		getExecutor().execute(() -> {

			ArrayList<BasicDBObject> list = new ArrayList<>();

			DBCursor cursor = getCollection().find();

			if (cursor != null) {

				while (cursor.hasNext()) {

					BasicDBObject obj = (BasicDBObject) cursor.next();
					list.add(obj);

				}
			}
			consumer.accept(list);

		});

	}

	public void setData(boolean update, String reportedUUID, ArrayList<String> reason, ArrayList<String> reporterUUID,
			String status, String teammitgliedUUID) {
		getExecutor().execute(() -> {
			BasicDBObject query = new BasicDBObject("uuid", reportedUUID);

			BasicDBObject found = (BasicDBObject) getCollection().findOne(query);

			BasicDBObject data = new BasicDBObject();
			data.append("uuid", reportedUUID);
			data.append("reasons", reason);
			data.append("reporter", reporterUUID);
			data.append("status", status);
			data.append("teamuuid", teammitgliedUUID);

			if (found == null || update == false) {
				getCollection().insert(data);
				return;
			} else if (update == true) {
				getCollection().update(query, new BasicDBObject("$set", data));
			}
		});

	}

	public void getData(String uuid, Consumer<BasicDBObject> consumer) {
		getExecutor().execute(() -> {
			BasicDBObject query = new BasicDBObject("uuid", uuid);
			BasicDBObject found = (BasicDBObject) getCollection().findOne(query);

			consumer.accept(found);

		});

	}

	public DBCollection getCollection() {
		return collection;
	}

	public String getIp() {
		return ip;
	}

	public Integer getPort() {
		return port;
	}

	public MongoClient getMongoClient() {
		return client;
	}

	public DB getDatabase() {
		return database;
	}

	public Executor getExecutor() {
		return executor;
	}

}
