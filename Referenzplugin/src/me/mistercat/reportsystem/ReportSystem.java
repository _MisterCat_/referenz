package me.mistercat.reportsystem;

import java.util.ArrayList;
import java.util.UUID;
import java.util.function.Consumer;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.mongodb.BasicDBObject;

import me.mistercat.commands.ReportCommand;
import me.mistercat.commands.ReportlistCommand;
import me.mistercat.database.MongoDB;
import me.mistercat.inventory.AcceptCloseInventory;
import me.mistercat.inventory.ReportlistInventory;
import me.mistercat.listener.InventoryClick;
import me.mistercat.listener.PlayerQuit;
import me.mistercat.manager.Manager;
import me.mistercat.manager.ReportManager;
import me.mistercat.manager.TeamManager;

public class ReportSystem extends JavaPlugin {

	private static ReportSystem INSTANCE;

	private TeamManager teamManager;
	private String prefix;
	private Manager manager;
	private ReportManager reportManager;
	private MongoDB mongoDB;
	private ReportlistInventory reportlistInventory;
	private AcceptCloseInventory acceptCloseInventory;

	@Override
	public void onEnable() {
		INSTANCE = this;
		prefix = "§8[§4Report§8] §a";
		manager = new Manager();
		teamManager = new TeamManager();
		reportManager = new ReportManager();
		reportlistInventory = new ReportlistInventory();
		acceptCloseInventory = new AcceptCloseInventory();
		mongoDB = new MongoDB("", 123, "");
		mongoDB.connectMongoDB();
		registerCommands();
		getServer().getPluginManager().registerEvents(new PlayerQuit(), this);
		getServer().getPluginManager().registerEvents(new InventoryClick(), this);

	}

	private void registerCommands() {
		getCommand("report").setExecutor(new ReportCommand());
		getCommand("reportlist").setExecutor(new ReportlistCommand());

	}

	public static ReportSystem getInstance() {
		return INSTANCE;
	}

	public TeamManager getTeamManager() {
		return teamManager;
	}

	public String getPrefix() {
		return prefix;
	}

	public Manager getManager() {
		return manager;
	}

	public ReportManager getReportManager() {
		return reportManager;
	}

	public MongoDB getMongoDB() {
		return mongoDB;
	}

	public ReportlistInventory getReportlistInventory() {
		return reportlistInventory;
	}

	public AcceptCloseInventory getAcceptCloseInventory() {
		return acceptCloseInventory;
	}

	public static void banPlayer(Player teamler, UUID uuid) {
		ReportSystem.getInstance().getMongoDB().getData(uuid.toString(), new Consumer<BasicDBObject>() {

			@SuppressWarnings("unchecked")
			@Override
			public void accept(BasicDBObject t) {

				if (t != null) {
					ArrayList<String> reporterList;
					ArrayList<String> reasonsList;

					reporterList = (ArrayList<String>) t.get("reporter");
					reasonsList = (ArrayList<String>) t.get("reasons");
					
					ReportSystem.getInstance().getMongoDB().setData(true, uuid.toString(), reasonsList, reporterList, "COMPLETE", teamler.getUniqueId().toString());
					teamler.sendMessage(ReportSystem.getInstance().getPrefix() + "Du hast den Report erfolgreich beendet!");

				}

			}
		});
	}
}
